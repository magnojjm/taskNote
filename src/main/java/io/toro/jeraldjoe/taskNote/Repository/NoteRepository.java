package io.toro.jeraldjoe.taskNote.Repository;

import io.toro.jeraldjoe.taskNote.Model.NoteModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.OrderBy;
import java.util.List;

@Repository
public interface NoteRepository extends JpaRepository<NoteModel, Long>{


         List<NoteModel> findAllByOrderByIsNoteImportantAscNoteCreateDate();

         List<NoteModel> findAllByOrderByIsNoteImportantDescNoteCreateDate();

         List<NoteModel> findAllByOrderByNoteCreateDateAsc();

         List<NoteModel> findAllByOrderByNoteCreateDateDesc();

         List<NoteModel> findAllByOrderByNoteUpdateDateAsc();

         List<NoteModel> findAllByOrderByNoteUpdateDateDesc();


}
